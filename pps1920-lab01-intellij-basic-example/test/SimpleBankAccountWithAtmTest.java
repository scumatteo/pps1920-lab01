import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleBankAccountWithAtmTest {
    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccountWithAtm;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccountWithAtm = new SimpleBankAccountWithAtm(accountHolder, 0);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccountWithAtm.getBalance());
    }

    @Test
    void testSimpleDeposit() {
        bankAccountWithAtm.deposit(accountHolder.getId(), 100);
        assertEquals(100, bankAccountWithAtm.getBalance());
    }

    @Test
    void testDepositWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        assertEquals(99, bankAccountWithAtm.getBalance());
    }

    @Test
    void testWrongSimpleDeposit() {
        bankAccountWithAtm.deposit(accountHolder.getId(), 100);
        bankAccountWithAtm.deposit(2, 50);
        assertEquals(100, bankAccountWithAtm.getBalance());
    }

    @Test
    void testWrongDepositWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        bankAccountWithAtm.depositWithAtm(2, 50);
        assertEquals(99, bankAccountWithAtm.getBalance());
    }
    @Test
    void testSimpleWithdraw() {
        bankAccountWithAtm.deposit(accountHolder.getId(), 100);
        bankAccountWithAtm.withdraw(accountHolder.getId(), 70);
        assertEquals(30, bankAccountWithAtm.getBalance());
    }

    @Test
    void testWithdrawWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        bankAccountWithAtm.withdrawWithAtm(accountHolder.getId(), 70);
        assertEquals(28, bankAccountWithAtm.getBalance());
    }


    @Test
    void testWrongSimpleWithdraw() {
        bankAccountWithAtm.deposit(accountHolder.getId(), 100);
        bankAccountWithAtm.withdraw(2, 70);
        assertEquals(100, bankAccountWithAtm.getBalance());
    }

    @Test
    void testWrongWithdrawWithAtm() {
        bankAccountWithAtm.depositWithAtm(accountHolder.getId(), 100);
        bankAccountWithAtm.withdrawWithAtm(2, 70);
        assertEquals(99, bankAccountWithAtm.getBalance());
    }
}
