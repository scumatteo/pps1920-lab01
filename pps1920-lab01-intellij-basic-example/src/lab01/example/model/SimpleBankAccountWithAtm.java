package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {
    private static final int ATM = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(final int usrID, final double amount) {
        this.deposit(usrID, amount - ATM);
    }

    @Override
    public void withdrawWithAtm(final int usrID, final double amount) {
        this.withdraw(usrID, amount + ATM);
    }


}
