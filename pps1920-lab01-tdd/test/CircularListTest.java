import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
/**
 * The test suite for testing the CircularList implementation
 */
class CircularListTest {

    private static final int START_RANGE_ELEMENTS = 0;
    private static  final int END_RANGE_ELEMENTS = 10;
    private static final int MULTIPLE_NUMBER = 3;
    private static final int COMPARISON_NUMBER = 5;


    private CircularList circularList;
    private List<Integer> elements;
    private StrategyFactory strategyFactory = new StrategyFactoryImpl();

    @BeforeEach
    void beforeEach(){
        this.circularList = new CircularListImpl();
        this.elements = IntStream.range(START_RANGE_ELEMENTS, END_RANGE_ELEMENTS)
                .boxed().collect(Collectors.toList());
    }

    @Test
    void testInitialList() {
        assertTrue(this.circularList.isEmpty());
    }

    @Test
    void testInitialSize(){
        assertEquals(0, this.circularList.size());
    }

    @Test
    void testAddElements(){
        this.fillCircularList();
        assertFalse(this.circularList.isEmpty());
    }

    @Test
    void testListSize(){
        this.fillCircularList();
        assertEquals(this.elements.size(), this.circularList.size());
    }

    @Test
    void testNextElement(){
        this.fillCircularList();
        this.elements.addAll(this.elements);
        Iterator<Integer> elementsIterator = this.elements.iterator();
        while (elementsIterator.hasNext()){
            assertEquals(elementsIterator.next(), this.circularList.next().get());
        }
    }

    @Test
    void testPreviousElement(){
        this.fillCircularList();
        Collections.reverse(this.elements);
        this.elements.addAll(this.elements);
        Iterator<Integer> elementsIterator = this.elements.iterator();
        while (elementsIterator.hasNext()){
            assertEquals(elementsIterator.next(), this.circularList.previous().get());
        }
    }

    @Test
    void testResetElement(){
        this.fillCircularList();
        this.circularList.next();
        this.circularList.reset();
        this.circularList.previous();
        assertEquals(this.circularList.next().get(), this.circularList.next().get());
    }

    @Test
    void testSingleEvenStrategy(){
        this.fillCircularList();
        this.testEvenStrategyElement();
    }
    @Test
    void testSingleMultipleOfStrategy(){
        this.fillCircularList();
        this.testMultipleOfStrategyElement();
    }
    @Test
    void testSingleComparisonStrategy(){
        this.fillCircularList();
        this.testComparisonStrategyElement();
    }

    @Test
    void testStrategyMixin(){
        this.fillCircularList();
        List<Integer> numberList = Arrays.asList(START_RANGE_ELEMENTS, MULTIPLE_NUMBER, COMPARISON_NUMBER);
        List<SelectStrategy> strategyList = Arrays.asList(this.strategyFactory.createEvenStrategy(),
                this.strategyFactory.createMultipleOfStrategy(MULTIPLE_NUMBER),
                this.strategyFactory.createComparisonStrategy(COMPARISON_NUMBER));

        numberList.stream().forEach(e ->
                assertEquals(e, this.circularList.next(strategyList.get(numberList.indexOf(e))).get()));
    }

    @Test
    void testEvenStrategyFactor(){
        assertTrue(this.strategyFactory.createEvenStrategy() instanceof EvenStrategyImpl);
    }

    @Test
    void testMultipleOfStrategyFactor(){
        assertTrue(this.strategyFactory.createMultipleOfStrategy(MULTIPLE_NUMBER) instanceof MultipleOfStrategyImpl);
    }

    @Test
    void testComparisonStrategyFactor(){
        assertTrue(this.strategyFactory.createComparisonStrategy(COMPARISON_NUMBER) instanceof ComparisonStrategyImpl);
    }

    private void testEvenStrategyElement(){
        this.elements.stream().filter(e -> e % 2 == 0).
                forEach(i -> assertEquals(i, this.circularList.next(this.strategyFactory.createEvenStrategy()).get()));
    }


    private void testMultipleOfStrategyElement(){
        this.elements.stream().filter(e -> e % MULTIPLE_NUMBER == 0).
                forEach(i -> assertEquals(i, this.circularList.next(this.strategyFactory.
                        createMultipleOfStrategy(MULTIPLE_NUMBER)).get()));
    }


    private void testComparisonStrategyElement(){
        this.elements.stream().filter(e -> e == COMPARISON_NUMBER).
                forEach(i -> assertEquals(i, this.circularList.next(this.strategyFactory.
                        createComparisonStrategy(COMPARISON_NUMBER)).get()));
    }

    private void fillCircularList(){
        this.elements.forEach(e -> this.circularList.add(e));
    }

}
