package lab01.tdd;

public class StrategyFactoryImpl implements StrategyFactory {

    @Override
    public SelectStrategy createEvenStrategy() {
        return new EvenStrategyImpl();
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int number) {
        return new MultipleOfStrategyImpl(number);
    }

    @Override
    public SelectStrategy createComparisonStrategy(int number) {
        return new ComparisonStrategyImpl(number);
    }
}
