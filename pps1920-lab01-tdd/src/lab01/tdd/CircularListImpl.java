package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList{

    private static final int INITIAL_INDEX = 0;

    private List<Integer> linkedList;
    private int currentIndex;

    public CircularListImpl(){
       this.linkedList = new LinkedList<>();
       this.currentIndex = INITIAL_INDEX;
    }

    @Override
    public void add(int element) {
        this.linkedList.add(element);
    }

    @Override
    public int size() {
        return this.linkedList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.linkedList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        int index = this.currentIndex;
        this.incrementIndex();
        return Optional.of(this.linkedList.get(index));
    }

    @Override
    public Optional<Integer> previous() {
        this.currentIndex--;
        if(this.currentIndex < INITIAL_INDEX){
            this.currentIndex = this.size() + this.currentIndex;
        }
        return Optional.of(this.linkedList.get(this.currentIndex));
    }

    @Override
    public void reset() {
        this.linkedList.set(this.currentIndex, this.linkedList.get(INITIAL_INDEX));
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        int index = this.currentIndex;
        Optional<Integer> nextElement = this.findFirstCorrect(strategy);
        if(nextElement.isPresent()){
           this.updateIndexIfPresent(nextElement.get());
        }
        else{
            this.currentIndex = INITIAL_INDEX;
            nextElement = this.findFirstCorrect(strategy);
            if(nextElement.isPresent()){
                this.updateIndexIfPresent(nextElement.get());
            }
            else{
                this.currentIndex = index;
            }
        }
        return nextElement;
    }

    private Optional<Integer> findFirstCorrect(SelectStrategy strategy){
        return this.linkedList.stream().
                skip(this.currentIndex).
                filter(strategy::apply).findFirst();
    }

    private void updateIndexIfPresent(final Integer nextElement){
        this.currentIndex = this.linkedList.indexOf(nextElement);
        this.incrementIndex();
    }

    private void incrementIndex(){
        this.currentIndex = (this.currentIndex + 1) % this.size();
    }
}
