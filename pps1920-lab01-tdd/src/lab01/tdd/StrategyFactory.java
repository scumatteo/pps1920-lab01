package lab01.tdd;

public interface StrategyFactory {

    SelectStrategy createEvenStrategy();

    SelectStrategy createMultipleOfStrategy(final int number);

    SelectStrategy createComparisonStrategy(final int number);
}
