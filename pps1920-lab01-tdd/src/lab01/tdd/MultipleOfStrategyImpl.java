package lab01.tdd;

public class MultipleOfStrategyImpl implements SelectStrategy{
    private final int number;

    public MultipleOfStrategyImpl(final int number){
        this.number = number;
    }

    @Override
    public boolean apply(int element) {
        return element % this.number == 0;
    }
}
