package lab01.tdd;

public class ComparisonStrategyImpl implements  SelectStrategy{
    private final int number;

    public ComparisonStrategyImpl(final int number){
        this.number = number;
    }

    @Override
    public boolean apply(int element) {
        return this.number == element;
    }
}
